﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PuntoFLexometro : MonoBehaviour {


    [SerializeField]
    private generarDatosPoste subPuntoCompañeroDependiente;
    [SerializeField]
    private int tipoDeRango;

    [SerializeField]
    private GameObject subPuntoCompañero;
    [SerializeField]
    private float rangoMaximo;

    [SerializeField]
    private float rangoMinimo;

    [SerializeField]
    private float valorAceptado;

    /// <summary>
    /// se pone en tue si se es mayor al valor aceptado dado 
    /// </summary>
    [SerializeField]
    private bool EsMayor;

    [SerializeField]
    private ManagerFlexometro MgFlexometro;

    /// <summary>
    /// es el indice que ocupa ene l vector del ManagerFlexometro
    /// </summary>
    [SerializeField]
    private int IndexDeestepunto;

    [SerializeField]
    private  Image ImgFlexometro;

    /// <summary>
    /// distancia que aparesera de manera aleatoria para el usuario
    /// </summary>
    private float DistanciaAsignada;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    /// <summary>
    /// activa el punto compañero y desactiva los demas
    /// </summary>
    public void activarPuntoCompañero(bool activo) {

        subPuntoCompañero.SetActive(activo);
        MgFlexometro.DesactivarCompañeros(IndexDeestepunto);

    }

    /// <summary>
    /// crea la distancia que le va a salir a el usuario en el panel del flesometro
    /// </summary>
    public void generarDistanciaDelPunto()
    {
       
        switch (tipoDeRango)
        {

            case 0:
                DistanciaAsignada = Random.Range(rangoMinimo, rangoMaximo);
                break;
            case 1:
                ///e(Es la altura visible)-valoresrandon
                var valorMinimo1 = subPuntoCompañeroDependiente.alturaVisible- rangoMinimo;
                var valorMiximo1 = subPuntoCompañeroDependiente.alturaVisible - rangoMaximo;
                DistanciaAsignada = Random.Range(valorMinimo1, valorMiximo1);
                break;

            case 2:
                //valorRandon-h(es la profundidad de empotramiento)

                DistanciaAsignada = subPuntoCompañeroDependiente.alturaVisible;
                break;

            case 3:
                // (1/3)*( valorRandon-h)
              
                valorAceptado = 0.3333333f*(subPuntoCompañeroDependiente.alturaVisible);
                DistanciaAsignada = 0.33333f*(8.4f- subPuntoCompañeroDependiente.ProfundidadEmpotramiento);
                Debug.Log(subPuntoCompañeroDependiente.alturaVisible);
                Debug.Log(subPuntoCompañeroDependiente.ProfundidadEmpotramiento);
                break;

            case 4:
                // (1/2)*(valorRandon-h)
                valorAceptado = 0.666666f* subPuntoCompañeroDependiente.alturaVisible;
                DistanciaAsignada = Random.Range(0.66f,0.68f) *  subPuntoCompañeroDependiente.alturaVisible;
                break;

        }


    }

    /// <summary>
    /// Este metodo retorna si el valor que se genero es correcto segun la variable valor aceptado y EsMayor 
    /// </summary>
    /// <returns></returns>
    public bool EsCorrectoELValorDado()
    {

        if (valorAceptado <= DistanciaAsignada)
        {
            if (EsMayor)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            if (EsMayor)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

    }

    public void PintarValor()
    {
        MgFlexometro.pintarMedida(DistanciaAsignada);
        MgFlexometro.desactivarImgenDepuntosFLexometro();
        MgFlexometro.ActivarCompañeros(IndexDeestepunto);
        subPuntoCompañero.SetActive(false);
        ImgFlexometro.gameObject.SetActive(true);
       
    }

    public void desactivarImagenFlexometro()
    {
        ImgFlexometro.gameObject.SetActive(false);
    }


    public float getVolorObtenidoAleatoriaMente()
    {
        return DistanciaAsignada;
    }



}
