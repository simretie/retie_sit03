﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalculoResistencia : MonoBehaviour {

	// Use this for initialization
	[Header("Calculo Resistencia")]
	public float MaxOhmio = 20;
	public int CasoInicial = 0;
	public bool ZonaError = false;

	[Header("Intervalor Var Aleatoria")]
	public float[] IntervaloMaxOhmio;		// porcentajes entero de 1 a 3 cifras 
	public float[] PorcentajesPosiciones;

	private float RandomOhmio;
	private float[] datos;

	[Header("Salida datos")]
	public TMPro.TMP_InputField TextoResistencia;
	public TMPro.TextMeshPro Resistencia61;

	[Header("Zona Calificar")]
	public int MiZona = 0;
	private CalificarRetie2 calificar;

	public void Init () {

		datos = new float[9];
		PorcentajesPosiciones = new float[9];
		PorcentajesPosiciones [0] = 0f;
		PorcentajesPosiciones [1] = 0.017f;
		PorcentajesPosiciones [2] = 0.019f;
		PorcentajesPosiciones [3] = 0.019f;
		PorcentajesPosiciones [4] = 0.019f;
		PorcentajesPosiciones [5] = 0.0163f;
		PorcentajesPosiciones [6] = 0.0193f;
		PorcentajesPosiciones [7] = 0.0228f;
		PorcentajesPosiciones [8] = 0.05f;

		calificar = GameObject.FindObjectOfType<CalificarRetie2> ();

	}
	
	// Update is called once per frame
	void Update () {
		
	}public void calcularResitencia(float dato, int index, float distanciaTotal, float resistenciaSPT, int zonaerrorselected = -1){ // resistencia es un % en decimal

		int desface = 0;
		if(!ZonaError){
			desface = 0;
		}
		if(ZonaError && GetComponent<Telurometro>().GetZonaSelected() == GetComponent<Telurometro>().GetzonaError()){
			desface = 6;
		}
		Get61PercentOhmio(resistenciaSPT);
		float datoReturn = (RandomOhmio+desface)+(RandomOhmio*PorcentajesPosiciones[index])-(RandomOhmio * resistenciaSPT) + ((Random.Range(-3.0f,3.0f)/100.0f)*desface); // calculo sin porcentaje de error
 		TextoResistencia.text = datoReturn.ToString("F2");
  		datos[index] = datoReturn;

		/*
		if(index >= 8)
			calificar.SetMaxResistenciaZona(MiZona,float.Parse(datoReturn.ToString("0.00")));

		*/

		// indicación de los valores por pica medida
		Telurometro TempDibujarMedida;
		TempDibujarMedida = GetComponent<Telurometro>();
		TempDibujarMedida.ReferenciasMedidas[index].gameObject.SetActive(true);
		if(index!=5){
			TempDibujarMedida.ReferenciasMedidas[index].transform.GetChild(2).GetComponent<TMPro.TextMeshProUGUI>().text = ((index+1)*10).ToString("F1");
		}else{
			TempDibujarMedida.ReferenciasMedidas[index].transform.GetChild(2).GetComponent<TMPro.TextMeshProUGUI>().text = "61.8";

			if(zonaerrorselected== -1)
				calificar.SetResistenciaMedia(MiZona,float.Parse(datoReturn.ToString("0.00"))); // resistencia media
			else
				calificar.SetResistenciaMedia(1,float.Parse(datoReturn.ToString("0.00"))); // resistencia media zona 1

		}TempDibujarMedida.ReferenciasMedidas[index].transform.GetChild(1).GetComponent<TMPro.TextMeshProUGUI>().text = dato.ToString("F2");
		TempDibujarMedida.ReferenciasMedidas[index].transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().text = datoReturn.ToString("F2");

	}public void RandomOhm(){

		RandomOhmio = Random.Range(MaxOhmio*(IntervaloMaxOhmio[0+(CasoInicial*2)]/100), MaxOhmio*(IntervaloMaxOhmio[1+(CasoInicial*2)]/100));

	}public float[] DatosParaGraficar(){

		return datos;

	}public void Get61PercentOhmio(float resistenciaSPT){

		int desface = 13;
		if(!ZonaError)
			desface = 0;

		float datoReturn = (RandomOhmio+desface)+((RandomOhmio*0.0158f)-(RandomOhmio * resistenciaSPT)); // calculo sin porcentaje de error
		//Resistencia61.text = datoReturn.ToString("F2");

		//calificar.SetResistenciaMedia(MiZona,float.Parse(datoReturn.ToString("0.00")));
 
	}public void SetZonaErrorSit0(int pos){

		if(pos == 0){

			ZonaError = true;
		}else{

			ZonaError = false;

		}

	}
}
