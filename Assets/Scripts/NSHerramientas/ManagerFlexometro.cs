﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSInterfaz;

public class ManagerFlexometro : MonoBehaviour {
    [SerializeField]
    private generarDatosPoste genValoresPoste;

    public GameObject[] puntosDeLaZona;

    [SerializeField]
    private PanelFlexometro panelFlexometro;

   public bool PuntosFlexometroActivos;


	// Use this for initialization
	void Start () {
        reiniciarPuntos();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    /// <summary>
    /// hace que sean visibles los puntos que se pueden tomar en esta zana
    /// </summary>
    public void activarPuntosFlexometro()
    {
        int tam = puntosDeLaZona.Length;
        if (!PuntosFlexometroActivos)
        {
            for (int i = 0; i < tam; i++)
                puntosDeLaZona[i].SetActive(true);

            PuntosFlexometroActivos = true;
            panelFlexometro.Mostrar(false);
        }
        else
        {
            for (int i = 0; i < tam; i++)
            {
                puntosDeLaZona[i].SetActive(false);
                puntosDeLaZona[i].GetComponent<PuntoFLexometro>().activarPuntoCompañero(false);
                puntosDeLaZona[i].GetComponent<PuntoFLexometro>().desactivarImagenFlexometro();
            }

            PuntosFlexometroActivos = false;
            panelFlexometro.Mostrar(false); 

        }


    }

    public void desactivarImgenDepuntosFLexometro()
    {        
        int tam = puntosDeLaZona.Length;
       
        for (int i = 0; i < tam; i++)
        {
            puntosDeLaZona[i].SetActive(false);
            puntosDeLaZona[i].GetComponent<PuntoFLexometro>().activarPuntoCompañero(false);
            puntosDeLaZona[i].GetComponent<PuntoFLexometro>().desactivarImagenFlexometro();
            Debug.Log("desactivarImgenDepuntosFLexometro");
        }        
    }


    public void DesactivarCompañeros(int PuntoADejarActivo)
    {
        int tam = puntosDeLaZona.Length;
        for (int i = 0; i < tam; i++)
        {
            if(i != PuntoADejarActivo)
                puntosDeLaZona[i].SetActive(false);
        }

    }


    public void ActivarCompañeros(int puntoAOcultar)
    {
        int tam = puntosDeLaZona.Length;
        for (int i = 0; i < tam; i++)
        {
            if (i != puntoAOcultar)
                puntosDeLaZona[i].SetActive(true);
            else
                puntosDeLaZona[i].SetActive(false);
        }

    }


    public void pintarMedida(float valor)
    {
        panelFlexometro.GetComponentInParent<PanelFlexometro>().MostrarValorEnPantallaFlexometro(valor);

    }


    public void reiniciarPuntos()
    {
        //genValoresPoste.ReiniciarValoresPoste();
        //StartCoroutine(reiniciar());
        reiniciarSimple();
        panelFlexometro.Mostrar(false);
    }

    IEnumerator reiniciar()
    {
        int tam = puntosDeLaZona.Length;
        yield return new WaitForSeconds(0.5f);
        for (int i = 0; i < tam; i++)
            puntosDeLaZona[i].GetComponent<PuntoFLexometro>().generarDistanciaDelPunto();
    }


    public void reiniciarSimple()
    {
        int tam = puntosDeLaZona.Length;
        for (int i = 0; i < tam; i++)
            puntosDeLaZona[i].GetComponent<PuntoFLexometro>().generarDistanciaDelPunto();
    }

}
