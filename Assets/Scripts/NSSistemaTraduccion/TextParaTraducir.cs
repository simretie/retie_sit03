﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace NSTraduccionIdiomas
{
	public class TextParaTraducir : Text
	{
        #region members

        private string textoOriginalEspaniol;
        #endregion

        #region accesors

        public string _textoOriginalEspaniol
        {
            set
            {
                textoOriginalEspaniol = value;
                text = value;
                DiccionarioIdiomas._instance.Traducir(this.text, this.text);
            }
            get
            {
                return textoOriginalEspaniol;
            }            
        }

        #endregion

        #region monoBehaviour

        protected override void Awake()
        {
            base.Awake();                       
        }

        protected override void Start()
        {
            base.Start();
            textoOriginalEspaniol = text;
        }


        protected override void OnEnable()
        {
            base.OnEnable();
            DiccionarioIdiomas._instance.Traducir(this);
        }

        #endregion
    }
}