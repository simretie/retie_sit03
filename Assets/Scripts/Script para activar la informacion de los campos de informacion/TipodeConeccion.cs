﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TipodeConeccion : MonoBehaviour {

    public MangerZonas mgZonas;
    public GameObject lineaTrenzada;
    public GameObject LineaAbierta;

    public puntosEvaluacion[]ObjetosAevaluar;

    public PuntoFLexometro[] puntosLineaTrenzada;

    public PuntoFLexometro[] puntoLineAbierta;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void activarLinea() {

        if (mgZonas.EsTrenzado)
        {
            LineaAbierta.SetActive(false);
            lineaTrenzada.SetActive(true);
            puntosEvaluacionLineaTenzada();
        
        }
        else
        {
            lineaTrenzada.SetActive(false);
            LineaAbierta.SetActive(true);
            puntosEvaluacionLineaAbierta(); 
        }

    }

    public void reiniciar()
    {
        lineaTrenzada.SetActive(false);
        LineaAbierta.SetActive(false);
    }

    private void puntosEvaluacionLineaTenzada()
    {
        var tam = puntosLineaTrenzada.Length;
        for (int i=0;i<tam;i++){
             ObjetosAevaluar[i].puntoDelFlexometro[0]= puntosLineaTrenzada[i];
        }
    }

    private void puntosEvaluacionLineaAbierta()
    {
        var tam = puntoLineAbierta.Length;
        for (int i = 0; i < tam; i++)
        {
            ObjetosAevaluar[i].puntoDelFlexometro[0] = puntoLineAbierta[i];
        }

    }


}

