﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSInterfaz;
using NSBoxMessage;
using NSTraduccionIdiomas;

public class controladorVentanadeBienvenida : MonoBehaviour {

    public PanelInterfazMensaje VentanaInformacion;
    public MangerZonas MgZonas;
    public ControladorDeDatos ContDatos;
    public clsCuaderno cuaderno;

	// Use this for initialization
	void Start () {
      
       
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void activarBienvenida()
    {
        cuaderno.initCuadreno();
        cuaderno.mtdPasarInfoAlPdf();
        VentanaInformacion.Mostrar(true);
    }
    public void BtnReiniciarSituacion()
    {
        BoxMessageManager._instance.MtdCreateBoxMessageDecision(DiccionarioIdiomas._instance.Traducir("MensajeReiniciarSituacion", "¿Está seguro que desea reiniciar la práctica?"), DiccionarioIdiomas._instance.Traducir("TextBotonAceptar", "ACEPTAR"), DiccionarioIdiomas._instance.Traducir("TextBotonCancelar", "CANCELAR"), reiniciarSituacion);
	}public void BtnReiniciarSituacionR2()
	{
		BoxMessageManager._instance.MtdCreateBoxMessageDecision(DiccionarioIdiomas._instance.Traducir("MensajeReiniciarSituacion", "¿Está seguro que desea reiniciar la práctica?"), DiccionarioIdiomas._instance.Traducir("TextBotonAceptar", "ACEPTAR"), DiccionarioIdiomas._instance.Traducir("TextBotonCancelar", "CANCELAR"), reiniciarSituacionR2);
	}public void BtnReiniciarSituacionR3()
	{
		BoxMessageManager._instance.MtdCreateBoxMessageDecision(DiccionarioIdiomas._instance.Traducir("MensajeReiniciarSituacion", "¿Está seguro que desea reiniciar la práctica?"), DiccionarioIdiomas._instance.Traducir("TextBotonAceptar", "ACEPTAR"), DiccionarioIdiomas._instance.Traducir("TextBotonCancelar", "CANCELAR"), reiniciarSituacionR3);
	}public void BtnReiniciarSituacionR4()
	{
		BoxMessageManager._instance.MtdCreateBoxMessageDecision(DiccionarioIdiomas._instance.Traducir("MensajeReiniciarSituacion", "¿Está seguro que desea reiniciar la práctica?"), DiccionarioIdiomas._instance.Traducir("TextBotonAceptar", "ACEPTAR"), DiccionarioIdiomas._instance.Traducir("TextBotonCancelar", "CANCELAR"), reiniciarSituacionR4);
	}


    private void reiniciarSituacion()
    {
        MgZonas.ReiniciarMapa();
        ContDatos.IniciarNuevaSesionSituacion();

	}private void reiniciarSituacionR2()
	{
		GameObject.FindObjectOfType<ManagerRetieZonas2>().ReinicirZonas();
		ContDatos.IniciarNuevaSesionSituacion();

	}public void reiniciarSituacionR3()
	{
		GameObject.FindObjectOfType<CalificarRetie3>().Reiniciar();
		ContDatos.IniciarNuevaSesionSituacion();

	}private void reiniciarSituacionR4()
	{
		GameObject.FindObjectOfType<CalificarRetie4>().Reiniciar();
		ContDatos.IniciarNuevaSesionSituacion();

	}


}
