﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class puntosEvaluacion: MonoBehaviour {

        
    public PuntoFLexometro[] puntoDelFlexometro;
    public int IdAlquePertenece;
    public bool activo;

    private void Start()
    {
       // activo = true;
    }

    /// <summary>
    /// retorna 1 si todos los puntos del vector tiene el mismo valor 
    /// </summary>
    /// <returns></returns>
    public float calcularValorCorrecto(bool respuesta)
    {
       int valorARetornar=0;
        int tam = puntoDelFlexometro.Length;
       // var valorPorRespuestaCorrecta = tam*1f;
        for (int i=0;i<tam;i++)
        {
            if (puntoDelFlexometro[i].EsCorrectoELValorDado() == respuesta)
            {
                valorARetornar = +1;
            }
            else
            {
                valorARetornar = valorARetornar - 1;
            }
        }
        if (1 == valorARetornar) {
            return 1;
        }
        else {
            return 0f;//valorARetornar;
        }
        
    } 




}
