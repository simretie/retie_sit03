﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

using UnityEngine.UI;

public class EvaluadorDeZona : MonoBehaviour {


    public float ValorDePreguntaBuena;

    public TMP_Dropdown[] RespuestasUsuario;
    public puntosEvaluacion[] puntosAEvaluar;


    [SerializeField]
    private float calificacion;

    // Use this for initialization
    void Start() {
        calificacion = 0;
    }

    // Update is called once per frame
    void Update() {

    }

    /// <summary>
    /// suma el valor de la respuesta correta por cada tipo de punto donde valorDeRespuesta(variable de la clase) es lo que vale una respuesta  buena 
    /// </summary>
    public float Get_valorPorcentajeCorreto()
    {
        calificacion = 0f;
        var tam = puntosAEvaluar.Length;
        for (int i = 0; i < tam; i++)
        {

     
            var indexDerespuestaUsuario = puntosAEvaluar[i].IdAlquePertenece;
            switch (RespuestasUsuario[indexDerespuestaUsuario].value)
            {
                case 0:
                    colorearCampo(0, indexDerespuestaUsuario);
                    faltaPorllenarCampos();
                    break;
                case 1:
                    if (puntosAEvaluar[i].activo)
                    {
                        float resul = puntosAEvaluar[i].calcularValorCorrecto(true);

                        colorearCampo(resul, indexDerespuestaUsuario);
                        calificacion = calificacion +( ValorDePreguntaBuena * resul);
                    }else
                    {
                        colorearCampo(0, indexDerespuestaUsuario);
                    }
                    break;
                case 2:
                    if (puntosAEvaluar[i].activo)
                    {
                        float resul2 = puntosAEvaluar[i].calcularValorCorrecto(false);
                        colorearCampo(resul2, indexDerespuestaUsuario);
                        calificacion = calificacion + (ValorDePreguntaBuena * resul2);
                      
                    }else
                    {
                        colorearCampo(0, indexDerespuestaUsuario);
                    }
                    break;
                case 3:
                    if (puntosAEvaluar[i].activo)
                    {
                        colorearCampo(0, indexDerespuestaUsuario);
                    }
                    else
                    {
                        calificacion = calificacion + ValorDePreguntaBuena;
                        colorearCampo(1, indexDerespuestaUsuario);
                    }

                    break;

            }
        }
        return calificacion;

    }


    /// <summary>
    /// se llama para activar la bandera de datos incompletos
    /// </summary>
    private void faltaPorllenarCampos()
    {
        GameObject.FindGameObjectWithTag("evaluacion").GetComponent<Calificar>().HayPregutnasSinResponder = true;
    }


    /// <summary>
    /// cambia el color dependiendo si al respuesta es correcta o no en el registro de datos
    /// </summary>
    /// <param name="resultadoDeValidarLaPregunta"></param>
    /// <param name="IdRespuestaUsuario"></param>
    private void colorearCampo(float resultadoDeValidarLaPregunta,int IdRespuestaUsuario) {

        var imagenX = RespuestasUsuario[IdRespuestaUsuario].transform.GetChild(0); //RespuestasUsuario[IdRespuestaUsuario].gameObject.GetComponent<Image>();
        if (resultadoDeValidarLaPregunta == 1f)
        {
            imagenX.gameObject.SetActive(false); // = new Color(115f / 255f, 236f / 255, 181f / 255f);
        }
        else
        {
             imagenX.gameObject.SetActive(true); //= new Color(236f / 255f, 152f / 255f, 115f / 255f);
        }
    }


}
