﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DocumentoRetie3 : MonoBehaviour {

	// Use this for initialization

	// datos random
	public int numeroCertificado = 0;
	public string fechaVencimiento = "";
	public string[] tipoProducto; 
	public string[] tipoCertificado;
	public string[] normas;

	// postes, verificar el activo para generar el certificado
	public GameObject[] postesActivos;

	private int actualNorma = 0;
	private int actualProducto = 0;
	// inputs
	public TMPro.TextMeshProUGUI NumCertAleatorio;
	public TMPro.TextMeshProUGUI FechaVencimiento;
	public TMPro.TextMeshProUGUI TipoProducto;
	public TMPro.TextMeshProUGUI TipoCertificado;
	public TMPro.TextMeshProUGUI Norma;

 	public GameObject OcultarBoton;
	int year = System.DateTime.Now.Year-1;
	int month = 06;
	int day = System.DateTime.Now.Day;
	private int intentos = 0;

	void RandomDay(int next = 0){

		year = System.DateTime.Now.Year-1;
		month = 06;
		day = System.DateTime.Now.Day;
		if (next >= 1) {
		
			year = System.DateTime.Now.Year +Random.Range(1, 2);
			month = Random.Range(1, System.DateTime.Now.Month);
			day = Random.Range(0,System.DateTime.Now.Day);
			Norma.text = normas[1];
			OcultarBoton.SetActive (false);

		}
		 
	}


	void Start () {

		Norma.text = normas [Random.Range (0, normas.Length)];
		Recalcular (); 
	}
	
	// Update is called once per frame
	void Update () {
		
	}public void Recalcular(){


		if(GameObject.FindObjectOfType<CalificarRetie3>().VecesCertificado >=1){
		
			RandomDay(5);
		
		}else{ 
			RandomDay();
		}
		int ran = Random.Range(0,normas.Length);
		if(actualNorma == ran){
			if(actualNorma == 0){

				ran +=1;
				if(ran >normas.Length){
					ran = 0;
				}

			}
		}

		if(intentos >= 1)
			Norma.text = normas[1]; 
			

		actualProducto = VerificarPoste();
		if(actualNorma == ran){
			if(actualNorma == 0){

				ran +=1;
				if(ran >tipoProducto.Length){
					ran = 0;
				}

			}
		}

		TipoProducto.text = tipoProducto[actualProducto];
		TipoCertificado.text = tipoCertificado[actualProducto];

		int ranHour = Random.Range(0,23);
		int randMinute = Random.Range(0,59);
		fechaVencimiento = year+"/"+month.ToString("00")+"/"+day.ToString("00") + " - "+ranHour.ToString("00")+":"+randMinute.ToString("00");
		FechaVencimiento.text = fechaVencimiento;

		numeroCertificado = Random.Range(0,99999);
		NumCertAleatorio.text = numeroCertificado.ToString();

		GameObject.FindObjectOfType<CalificarRetie3>().SetNumCertificado(numeroCertificado);
		intentos += 1;

	}public int VerificarPoste(){

		int pos = 0;
		for(var i = 0; i<postesActivos.Length; i++){

			if(postesActivos[i].activeSelf)
				pos = i;

		}

		return pos;
	}
}
