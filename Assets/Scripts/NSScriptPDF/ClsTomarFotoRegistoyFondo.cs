﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClsTomarFotoRegistoyFondo : MonoBehaviour
{

    [SerializeField]
    private Camera cameraCapturaImagenes;
    [SerializeField]
    private RenderTexture renderTextureImagenes;
    [SerializeField]
    private GameObject ObjetoContenedorDeZonas;

    public GameObject botonesyNumeros;
    public RawImage[] imagenesParapdf;
    public GameObject RegistrDt;
   // public GameObject fondo;



    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void capturarRegistroyFOndo()
    {

        StartCoroutine(CouCapturarImageFOndoYRegistroDeDatos());
    }


    private IEnumerator CouCapturarImageFOndoYRegistroDeDatos()
    {

        cameraCapturaImagenes.gameObject.SetActive(true);

        yield return new WaitForSeconds(1);


        for (int i = 0; i < imagenesParapdf.Length; i++)
        {
            if (i == 1)
            {
                ObjetoContenedorDeZonas.SetActive(false);
                botonesyNumeros.SetActive(false);
                yield return new WaitForEndOfFrame();
                cameraCapturaImagenes.Render();
                RenderTexture.active = renderTextureImagenes;
                var tmpTexture2D = new Texture2D(1919, 1079, TextureFormat.ARGB32, false, true);
                tmpTexture2D.ReadPixels(new Rect(0, 0, 1919, 1079), 0, 0);
                tmpTexture2D.Apply();
                imagenesParapdf[i].texture = tmpTexture2D;
                RenderTexture.active = null;
                ObjetoContenedorDeZonas.SetActive(true);
                botonesyNumeros.SetActive(true);
            }
            else
            {
                var clonRegistro= Instantiate(RegistrDt, imagenesParapdf[i].gameObject.transform);
                 clonRegistro.transform.localPosition = new Vector3(0f, 0f, 0f);
                 clonRegistro.transform.localScale= new Vector3(0.5f, 0.5f,0.5f);
                 clonRegistro.SetActive(true);
            }
        }
        cameraCapturaImagenes.gameObject.SetActive(false);  
    }

}
