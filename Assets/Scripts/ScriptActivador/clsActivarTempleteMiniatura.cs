﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class clsActivarTempleteMiniatura : MonoBehaviour {

    public GameObject templeteGuitarra;
    public GameObject templeteDirecto;
    public bool Esguitarra;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnEnable()
    {
        if (Esguitarra)
        {
            templeteGuitarra.SetActive(true);
            templeteDirecto.SetActive(false);
        }
        else
        {
            templeteDirecto.SetActive(true);
            templeteGuitarra.SetActive(false);
        }
    }

}
