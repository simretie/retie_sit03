﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CalificarRetie3 : MonoBehaviour {

	// Use this for initialization
	public InputField Long_SPT; // longitud de varilla de puesta a tierra
	public InputField NumCertificadoText;
	public int PosIncorrectoCaja3 = 3;
	public int Num_Certificado = 0;



	[Header("Zona 1")]
	public SelectorRetie3[] CajasZona1;
	public int _z0MaterialIncorrecto = 0;
	public int _z0EstructuraCorrecta1 = 1;
	public int _z0EstructuraCorrecta2 = 3;
	public int _z0TuberiaCorrecta = 0;
	public Dropdown[] _z0SeleccionLongitud;
	public int _z0OpcionLongitudValida = 2;
	[Header("Zona 2")]
	public SelectorRetie3[] CajasZona2;
	public int _z1MaterialIncorrecto = 0;
	public int _z1TuberiaCorrecta = 0;
	public int _z1CableEncauchetadoCorrecto = 2;
	public int _z1AisladorCableCorrecto = 1;
	public int _z1AisladorCorrecto = 1;
	public int _z1DSPCorrecto = 0;
	public Dropdown[] _z1SeleccionLongitud;
	public int _z1OpcionLongitudValida = 2;
	[Header("Zona 3")]
	public SelectorRetie3[] CajasZona3;
	public int _z2TempleteCorrecto = 1;
	public int _z2EstructuraCorrecta1 = 1;
	public int _z2EstructuraCorrecta2 = 3;
	public int _z2AisladorCorrecto = 0;
	[Space]
	public int VecesCertificado = 0;
	public float calibreSPTConductor = 2.4f;
	public InputField[] Calibre_SPT;

	public float Calificacion = 0f;
	public bool camposCorrectos = true;
	public bool EmptyCampos = false;
	public float NotaTotal = 0f;

	[Header("Settings para reiniciar")]
	public GameObject[] ZonasPrincipales;
	public GameObject ZonsPBotones = null;
	public GameObject BotonesYNumeros = null;
	public GameObject[] ZonasAOcultar;
	public GameObject[] ZonasActivas;

	[Header("Valores para la formula")]
	public TEXDraw[] formulas; 
	private float[] valores = new float[3];
	private float resultadoFormula = 0f;

	void Start () {

		GenerarFormula ();

	}
	
	// Update is called once per frame
	void Update () {
		
	}public void Verificar(){

		camposCorrectos = true;
		EmptyCampos = false;
		Calificacion = 0;

		float[] notaZonas = new float[3];
		float notaRegistro = 0;

		////zona 0
		int z0MaterialBaseSeleccionado = CajasZona1[1].ActualSeleccionado;
		bool zona0MaterialCorrecto = CajasZona1[0].ActualSeleccionado == CajasZona1[1].ActualSeleccionado
		&& CajasZona1[1].ActualSeleccionado == CajasZona1[2].ActualSeleccionado
		&& CajasZona1[0].ActualSeleccionado != _z0MaterialIncorrecto; // Si el material no es Acero cobretizado 

		float fraccionNotaRegistro = 0.25f / 4;
		float[] fraccionNotaZona =
		{
			0.2f/4, // 20% en n elementos
			0.2f/7, // 20% en n elementos
			0.1f/3 // 10% en n elementos
		};


		// Calificación materiales de conector, base y cable
		if (zona0MaterialCorrecto)
		{
			// correcto
			notaZonas[0] += fraccionNotaZona[0];
		}

		bool zona0EstructuraDePosteCorrecta = CajasZona1[3].ActualSeleccionado == _z0EstructuraCorrecta1
		|| CajasZona1[3].ActualSeleccionado == _z0EstructuraCorrecta2;
		if (zona0EstructuraDePosteCorrecta)
		{
			// correcto
			notaZonas[0] += fraccionNotaZona[0];
		}

		bool zona0TuberiaCorrecta = CajasZona1[4].ActualSeleccionado == _z0TuberiaCorrecta;
		if (zona0TuberiaCorrecta)
		{
			// correcto
			notaZonas[0] += fraccionNotaZona[0];
		}
		bool z0LongitudCorrecta = _z0SeleccionLongitud[z0MaterialBaseSeleccionado].value == _z0OpcionLongitudValida;

		// Cuando se selecciona 2.4m en la longitud de la base
		if (z0LongitudCorrecta)
		{
			notaZonas[0] += fraccionNotaZona[0];
		}

		//// zona1
		int z1MaterialBaseSeleccionado = CajasZona2[7].ActualSeleccionado;
		bool zona1MaterialCorrecto = CajasZona2[7].ActualSeleccionado == CajasZona2[6].ActualSeleccionado
		&& CajasZona2[7].ActualSeleccionado == CajasZona2[5].ActualSeleccionado
		&& CajasZona2[7].ActualSeleccionado != _z1MaterialIncorrecto;

		if (zona1MaterialCorrecto)
		{
			// correcto
			notaZonas[1] += fraccionNotaZona[1];
		}

		// Material tubería correcto
		if (CajasZona2[0].ActualSeleccionado == _z1TuberiaCorrecta)
		{
			// correcto
			notaZonas[1] += fraccionNotaZona[1];
		}

		// Elección cable encauchetado correcta
		if (CajasZona2[1].ActualSeleccionado == _z1CableEncauchetadoCorrecto)
		{

			// correcto
			notaZonas[1] += fraccionNotaZona[1];

		}

		// Aislador ANSI 54-2 Seleccionado
		if (CajasZona2[2].ActualSeleccionado == _z1AisladorCableCorrecto)
		{

			// correcto
			notaZonas[1] += fraccionNotaZona[1];

		}

		// Aislador tipo plato seleccionado
		if (CajasZona2[3].ActualSeleccionado == _z1AisladorCorrecto)
		{

			// correcto
			notaZonas[1] += fraccionNotaZona[1];

		}

		// Descargadores tipo pala seleccionado
		if (CajasZona2[4].ActualSeleccionado == _z1DSPCorrecto)
		{

			// correcto
			notaZonas[1] += fraccionNotaZona[1];

		}

		bool z1LongitudCorrecta = _z1SeleccionLongitud[z1MaterialBaseSeleccionado].value == _z1OpcionLongitudValida;
		if (z1LongitudCorrecta)
		{
			notaZonas[1] += fraccionNotaZona[1];
		}

		// zona 3
		if (CajasZona3[0].ActualSeleccionado == _z2TempleteCorrecto)
		{

			// correcto
			notaZonas[2] += fraccionNotaZona[2];

		}

		if (CajasZona3[1].ActualSeleccionado == _z2EstructuraCorrecta1 || CajasZona3[1].ActualSeleccionado == _z2EstructuraCorrecta2)
		{

			// correcto
			notaZonas[2] += fraccionNotaZona[2];

		}

		// Aislador ANSI 54-1 seleccionado
		if (CajasZona3[2].ActualSeleccionado == _z2AisladorCorrecto)
		{
			notaZonas[2] += fraccionNotaZona[2];
		}

		string textoLongitudRegistro = Long_SPT.text;
		float valorLongitud = 0f;
		bool longitudSPTValida = false;
		if (float.TryParse(textoLongitudRegistro, out valorLongitud))
		{
			longitudSPTValida = valorLongitud >= 2.439999f && valorLongitud <= 2.45000001f;
		}


		if (longitudSPTValida)
		{
			// correcto
			notaRegistro += fraccionNotaRegistro;
			Long_SPT.gameObject.transform.Find("icono_error").GetComponent<Image>().enabled = false;
		}
		else
		{
			// incorrecto
			Long_SPT.gameObject.transform.Find("icono_error").GetComponent<Image>().enabled = true;
			camposCorrectos = false;

			if (Long_SPT.text == "")
			{
				EmptyCampos = true;
			}
		}

		string num1 = NumCertificadoText.text;
		if (num1.Length <= 0)
		{
			num1 = "1";
			EmptyCampos = true;
		}

		if (int.Parse(num1) == Num_Certificado)
		{
			// correcto
			NumCertificadoText.gameObject.transform.Find("icono_error").GetComponent<Image>().enabled = false;
			notaRegistro += fraccionNotaRegistro;
		}
		else
		{
			// incorrecto
			NumCertificadoText.gameObject.transform.Find("icono_error").GetComponent<Image>().enabled = true;
			camposCorrectos = false;
		}

		num1 = Calibre_SPT[0].text;

		if(num1.Length <= 0){

			num1 = "2000";
			EmptyCampos = true;

		}
		if(float.Parse(num1.ToString()) <= (resultadoFormula+(resultadoFormula*0.05f)) && float.Parse(num1.ToString()) >= (resultadoFormula-(resultadoFormula*0.05f))){//mcm

			// correcto
			Calibre_SPT[0].gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = false;
			notaRegistro += fraccionNotaRegistro;
 
		}else{

			// incorrecto
			Calibre_SPT[0].gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = true;
			camposCorrectos = false;
			if(Calibre_SPT[0].text == ""){

				EmptyCampos = true;

			}

		}if(Calibre_SPT[1].text == "2/0"){//mcm

			// correcto
			Calibre_SPT[1].gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = false;
			notaRegistro += fraccionNotaRegistro;
 
		}else{

			// incorrecto
			Calibre_SPT[1].gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = true;
			camposCorrectos = false;

			if(Calibre_SPT[1].text == ""){

				EmptyCampos = true;

			}

		}

		Debug.Log(string.Format("Nota registro: {0}", notaRegistro));

		Calificacion = notaRegistro;
		int length = notaZonas.Length;
		for (int i = 0; i < length; i++)
		{
			Debug.Log(string.Format("Nota zona {0}: {1}", i, notaZonas[i]));
			Calificacion += notaZonas[i];
		}

		Debug.Log(string.Format("Calificacion Final Simulador -> {0}", Calificacion));
	}public void SetNumCertificado(int num){

		VecesCertificado += 1;
		Num_Certificado = num;

	}public float VerificarDatosUsuario(){

		Verificar();
		return Calificacion;

	}public void Reiniciar(){

		GenerarFormula();
		SelectorRetie3[] selectores = Resources.FindObjectsOfTypeAll<SelectorRetie3>();

		if(ZonsPBotones){
			ZonsPBotones.SetActive(true);
			BotonesYNumeros.SetActive(true);
		}
		for(var i = 0; i< ZonasPrincipales.Length; i++){

			ZonasPrincipales[i].SetActive(true);

		}


		foreach(SelectorRetie3 selector in selectores){

			selector.gameObject.SetActive(true);
			selector.RandomStart();
			selector.GetComponent<panelZonas>().Mostrar(false);
			selector.gameObject.SetActive(false);
		}

		for(var i = 0; i< ZonasPrincipales.Length; i++){

			ZonasPrincipales[i].SetActive(false);

		}for(var i = 0; i< ZonasAOcultar.Length; i++){

			ZonasAOcultar[i].SetActive(false);

		}for(var i = 0; i< ZonasActivas.Length; i++){

			ZonasActivas[i].SetActive(true);

		}


		NumCertificadoText.gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = false;
		Calibre_SPT[1].gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = false;
		Calibre_SPT[0].gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = false;
		Long_SPT.gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = false;

		Long_SPT.text = "";
		NumCertificadoText.text = "";
		Calibre_SPT[0].text = "";
		Calibre_SPT[1].text = "";

	}public void GenerarFormula(){


		valores[0] = Random.Range(3f,6f);
		valores[1] = 7.06f;
		valores[2] = (Random.Range(3f,7f)/10f);

		formulas[0].text = valores[0].ToString("F2");
		formulas[1].text = valores[1].ToString();
		formulas[2].text = valores[2].ToString("F2");

		resultadoFormula = (valores[0]*valores[1])*Mathf.Sqrt(valores[2]);
		resultadoFormula = float.Parse(resultadoFormula.ToString("F2"));

	}
}
