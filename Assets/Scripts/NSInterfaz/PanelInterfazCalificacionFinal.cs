﻿using NSAvancedUI;
using NSEvaluacion;
using NsSeguridad;
using NSTraduccionIdiomas;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace NSInterfazAvanzada 
{
	public class PanelInterfazCalificacionFinal : AbstractPanelUIAnimation
	{
        #region members

        [SerializeField]
        private TextMeshProUGUI textValorCalificacion;

        [SerializeField]
        private TextMeshProUGUI textNombreUsuario;

        [SerializeField]
        private Calificar refEvaluacion;

        /*
        [SerializeField]
        private PanelInterfazBotonesInterfazAfuera refPanelInterfazBotonesInterfazAfuera;

        [SerializeField]
        private SistemaSolarManager refSistemaSolarManager;

        [SerializeField]
        private PanelInterfazSeleccionFuncion refPanelInterfazSeleccionFuncion;

        [SerializeField]
        private ControlCamaraSistemaSolar refControlCamaraSistemaSolar;

        [SerializeField]
        private ControlCamaraPrincipal refControlCamaraPrincipal;

        [SerializeField]
        private clsHerramientasNotas refHerramientasNotas;*/

        [SerializeField]
        private clsEnvioPdf refEnvioPdf;

        [SerializeField]
        private ClsSeguridad refClsSeguridad;

        [SerializeField, Tooltip("Frases de aliento")]
        private TextMeshProUGUI textFelicitacion1;

        [SerializeField]
        private TextMeshProUGUI textFelicitacion2;

        [SerializeField]
        private string[] felicitacionMas60Porciento;

        [SerializeField]
        private string[] felicitacionMenos60Porciento;
        #endregion

        #region accesors

        #endregion

        #region events

        #endregion

        #region monoBehaviour

        private void Start()
        {
           // refClsSeguridad = GameObject.FindGameObjectWithTag("Seguridad").GetComponent<ClsSeguridad>();
        }

        private void OnEnable()
        {
            refClsSeguridad = GameObject.FindGameObjectWithTag("Seguridad").GetComponent<ClsSeguridad>();
            SetTextValorCalificacion(refEvaluacion.GetCalificacionTotalRango());

            //Asignar textos calificacion
            if (refEvaluacion.GetCalificacionTotal() > 0.6f)
            {
                textFelicitacion1.text= DiccionarioIdiomas._instance.Traducir( felicitacionMas60Porciento[0], felicitacionMas60Porciento[0]);
                textFelicitacion2.text = DiccionarioIdiomas._instance.Traducir(felicitacionMas60Porciento[1], felicitacionMas60Porciento[1]);
            }
            else
            {
                textFelicitacion1.text = DiccionarioIdiomas._instance.Traducir(felicitacionMenos60Porciento[0], felicitacionMenos60Porciento[0]);
                textFelicitacion2.text = DiccionarioIdiomas._instance.Traducir(felicitacionMenos60Porciento[1], felicitacionMenos60Porciento[1]);
            }            

            var tmpDatosSesion = refClsSeguridad.GetDatosSesion();
            SetTextNombreUsuario(tmpDatosSesion[0]);
        }

        #endregion

        #region private methods

        private void SetTextValorCalificacion(string argValorCalificacion)
        {
            textValorCalificacion.text = argValorCalificacion;
        }

        private void SetTextNombreUsuario(string argNombreUsuario)
        {
            Debug.Log("este es el nombre"+ argNombreUsuario);
            textNombreUsuario.text = argNombreUsuario;
        }


        #endregion

        #region public methods

        public void OnButtonAceptar()
        {

           /* refPanelInterfazBotonesInterfazAfuera.ActivarPanel();
            refSistemaSolarManager.SalirSituacion();
            refPanelInterfazSeleccionFuncion.SalirSituacion();
            refControlCamaraPrincipal.SetAccionActual(AccionesCamaraPrincipal.enfoquePrimeraPersona);
            refControlCamaraPrincipal.SetOrthograpicCamera(false);
            refControlCamaraSistemaSolar.SetAccionActual(AccionesCamaraSistemaSolar.enfoquePrimeraPersona);
            refControlCamaraSistemaSolar.SetOrthograpicCamera(false);
            //refEnvioPdf.VisualizarPDF();*/
        }
        #endregion
    }
}