﻿using NSCreacionPDF;
using NSEvaluacion;
using NSTraduccionIdiomas;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using NSAvancedUI;
using NSInterfazAvanzada;
namespace NSInterfazAvanzada
{
    public class PanelInterfazEvaluacion :AbstractPanelUIAnimation
    {
        #region members
        /// <summary>
        /// Indice actual del panel que se esta mostrando, en el indice 0 siempre se muestra el panel del enunciado, desde el indice 1 en adelante se muestran las preguntas 1 2 3 4... respectivamente
        /// </summary>
        private int indicePanelNavegacionActual=1;

        /// <summary>
        /// Array con todos los paneles de las preguntas, el indice cero de este array siempre contiene el panel del enunciado
        /// </summary>
        private GameObject[] panelesNavegacion;

        [Header("Variables panel evaluacion"), Space(10)]
        /// <summary>
        /// Image a la que se le asigna la imagen representativa del enunciado
        /// </summary>
        [SerializeField]
        private Image imageEnunciado;

        /// <summary>
        /// Text al que se le asigna el texto del enunciado
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI textEnunciado;

        /// <summary>
        /// Referencia a la clase que controla y contiene todas las preguntas de evaluacion de la situacion actual
        /// </summary>
        [SerializeField]
        private ControladorPreguntasSituacionales refControladorPreguntasSituacionales;

        /// <summary>
        /// Transform padre de todos los paneles de las preguntas y enunciado
        /// </summary>
        [SerializeField]
        private Transform panelContenedorEnunciadoPreguntas;

        /// <summary>
        /// Boton para continuar al panel de la derecha
        /// </summary>
        [SerializeField]
        private Button buttonDerecha;

        /// <summary>
        /// Boton para continuar al panel de la izquierda
        /// </summary>
        [SerializeField]
        private Button buttonIzquierda;

        /// <summary>
        /// Prefabricado del panel que muestra una pregunta 
        /// </summary>
        [SerializeField]
        private GameObject prefabPanelPregunta;

        /// <summary>
        /// Sprite que se usa para indicar que una pregunta falta por responder
        /// </summary>
        [SerializeField]
        private Sprite botonFechaRojo;

        /// <summary>
        /// Sprite que se usa para restablecer el color del boton
        /// </summary>
        [SerializeField]
        private Sprite botonFechaAzul;

        /// <summary>
        /// para saber que panel de preguntas estan sin responder, true = panel sin responder
        /// </summary>
        private bool[] panelesPreguntaSinResponder;

        /// <summary>
        /// Boton para terminar la evaluacion, este solo es interactuable cuando el estudiante a respondido todas las preguntas
        /// </summary>
        [SerializeField]
        private Button buttonTerminar;

        /// <summary>
        /// Para guardar los indices de las respuestas correcta.
        /// cada indice de este array tiene relacion con el indice del panel de pregunta de donde se guardara el indice de la respuesta correcta
        /// </summary>
        private int[] indicesRespuestasCorrectas;

        /// <summary>
        /// Referencia a la clase que contiene todas las preguntas de la situacion que se esta evaluando
        /// </summary>
        private PreguntasSituacion refPreguntasSituacion;

        /// <summary>
        /// Referencia a la clase que contiene las calificaciones de las areas, en este caso para asignar la calificacion de las respuestas a las preguntas
        /// </summary>
        [SerializeField]
        private Calificar refEvaluacion;

        [SerializeField]
        private CanvasReportePDF refCanvasReportePDF;

        [SerializeField]
        private ControladorValoresPDF refControladorValoresPDF;

        /*[SerializeField]
        private PanelInterfazCalificacionFinal refPanelInterfazCalificacionFinal;*/
        #endregion

        #region monoBehaviour

        private void OnEnable()
        {
            AsignarDatosEvaluacion();
            CalcularEstadoBotonesDerechaIzquierda();
        }

        private void Update()
        {
            ActivarBotonTerminar();
        }
        #endregion

        #region private methods

        /// <summary>
        /// Inactiva el boton izquierda o derecha si ya se llego al limite de navegacion entre ventanas
        /// </summary>
        private void CalcularEstadoBotonesDerechaIzquierda()
        {
            //Restringe la interaccion del boton si ya se llego a los limites de navegacion
            if (indicePanelNavegacionActual == panelesNavegacion.Length - 1)
                buttonDerecha.interactable = false;
            else
                buttonDerecha.interactable = true;


            if (indicePanelNavegacionActual == 0)// adaptacion para funsonamiento en los simuladores de electricidad
                buttonIzquierda.interactable = false;
            else
                buttonIzquierda.interactable = true;


            //Asigna color rojo al boton si falta responder alguna pregunta en esa direccion
            var tmpIndicePanelPregunta = indicePanelNavegacionActual - 1;

            if (tmpIndicePanelPregunta + 1 < panelesPreguntaSinResponder.Length)
            {
                if (panelesPreguntaSinResponder[tmpIndicePanelPregunta + 1])
                    buttonDerecha.GetComponent<Image>().sprite = botonFechaRojo;
                else
                    buttonDerecha.GetComponent<Image>().sprite = botonFechaAzul;
            }

            if (tmpIndicePanelPregunta - 1 >= 0)// era mayor igual a 0 pero se cambio por uno para  la adaptacion de lso simuladores de electrica
            {
                if (panelesPreguntaSinResponder[tmpIndicePanelPregunta - 1])
                    buttonIzquierda.GetComponent<Image>().sprite = botonFechaRojo;
                else
                    buttonIzquierda.GetComponent<Image>().sprite = botonFechaAzul;
            }
            else
                buttonIzquierda.GetComponent<Image>().sprite = botonFechaAzul;

        }

        /// <summary>
        /// Asigna true al array de panelesPreguntaSinResponder, si el panel actual no tiene seleccionada una respuesta
        /// </summary>
        private void CalcularPanelPreguntaSinResponder()
        {
            /*
             Obtengo el toogle group del panel de preguntas activo y miro si tiene por lo menos un toggle seleccionado
             */

            if (indicePanelNavegacionActual == 0)
                return;

            panelesPreguntaSinResponder[indicePanelNavegacionActual - 1] = !panelesNavegacion[indicePanelNavegacionActual].transform.GetChild(1).GetComponent<ToggleGroup>().AnyTogglesOn();//en PanelPregunta/hijo 2 (PanelListaOpciones ) Esta cualquier toggle activado en el toggleGruop ?            
        }

        /// <summary>
        /// Carga los valores para la evaluacion y los asigna en la interfaz panel de evaluacion, carga el sprite del enunciado, el texto de enunciado, las preguntas y respuestas
        /// </summary>
        private void AsignarDatosEvaluacion()
        {
            ///eliminar preguntas anteriores

            for (int i = 1; i < panelContenedorEnunciadoPreguntas.childCount; i++)
                Destroy(panelContenedorEnunciadoPreguntas.GetChild(i).gameObject);

            //agregar preguntas nuevas

            refPreguntasSituacion = refControladorPreguntasSituacionales.GetPreguntasSituacion();//Consigo la clase que contiene las preguntas y el enunciado de la situacion

            panelesNavegacion = new GameObject[refPreguntasSituacion._preguntas.Length + 1];//creo el array con la cantidad de paneles totales navegables, cantidad de preguntas + enunciado
            panelesNavegacion[0] = panelContenedorEnunciadoPreguntas.GetChild(0).gameObject;//asigno que el primer panel de navegacion, sea el panel que contiene el enunciado
            panelesNavegacion[0].transform.GetChild(0).GetComponent<Image>().sprite = refPreguntasSituacion._imagenEnunciado;//asigno el sprite del enunciado
            panelesNavegacion[0].transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = DiccionarioIdiomas._instance.Traducir(refPreguntasSituacion._textoEnunciado, refPreguntasSituacion._textoEnunciado);//asigno el texto del enunciado

            indicesRespuestasCorrectas = new int[refPreguntasSituacion._preguntas.Length];

            for (int i = 0; i < refPreguntasSituacion._preguntas.Length; i++)//para cada una de las preguntas de la situacion
            {
                var tmpIndiceRespuestaCorrecta = Random.Range(0, 4);//selecciono un indice para ubicar la respuesta correcta
                indicesRespuestasCorrectas[i] = tmpIndiceRespuestaCorrecta;
                var tmpListaIndicesRespuestasFalsasSinAsignar = new List<int>();//Lista para saber cuales indices de respuestas estan sin asignar

                for (int k = 0; k < 3; k++)
                    tmpListaIndicesRespuestasFalsasSinAsignar.Add(k);//agrego los indices de las respuestas falsas

                panelesNavegacion[i + 1] = Instantiate(prefabPanelPregunta, panelContenedorEnunciadoPreguntas);//creo un nuevo panel de pregunta
                panelesNavegacion[i + 1].transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = DiccionarioIdiomas._instance.Traducir(refPreguntasSituacion._preguntas[i], refPreguntasSituacion._preguntas[i]);//le asigno el texto de pregunta al nuevo panel de pregunta--------------------------preguntas carajo---

                var tmpContenedorRespuestas = panelesNavegacion[i + 1].transform.GetChild(1);//Asigno el transform que contiene todos los paneles de navegacion

                for (int j = 0; j < tmpContenedorRespuestas.childCount; j++)//para cada uno de los toggles de respuesta posibles
                {
                    if (j == tmpIndiceRespuestaCorrecta)
                    {
                        if (string.IsNullOrEmpty(refPreguntasSituacion._respuestas[i]))
                        {
                            var tmpImageRespuesta = tmpContenedorRespuestas.GetChild(j).Find("ImageRespuesta").GetComponent<Image>();
                            tmpImageRespuesta.sprite = refPreguntasSituacion._respuestasImagen[i];//asigno el sprite de la funcion respuesta
                            tmpImageRespuesta.SetNativeSize();
                            tmpImageRespuesta.gameObject.SetActive(true);
                        }
                        else
                        {
                            var tmpTextRespuesta = tmpContenedorRespuestas.GetChild(j).Find("TextRespuesta").GetComponent<TextMeshProUGUI>();
                            tmpTextRespuesta.text = DiccionarioIdiomas._instance.Traducir(refPreguntasSituacion._respuestas[i], refPreguntasSituacion._respuestas[i]);//asigno el texto de la respuesta correcta
                            tmpTextRespuesta.gameObject.SetActive(true);
                        }
                        continue;
                    }

                    var tmpIndiceRamdom = Random.Range(0, tmpListaIndicesRespuestasFalsasSinAsignar.Count);//tomo un indice ramdon de la lista

                    if (string.IsNullOrEmpty(refPreguntasSituacion._contenedorRespuestasFalsas[i]._respuestasFalsas[tmpListaIndicesRespuestasFalsasSinAsignar[tmpIndiceRamdom]]))
                    {
                        var tmpImageRespuesta = tmpContenedorRespuestas.GetChild(j).Find("ImageRespuesta").GetComponent<Image>();
                        tmpImageRespuesta.sprite = refPreguntasSituacion._contenedorRespuestasFalsas[i]._respuestasFalsasImagen[tmpListaIndicesRespuestasFalsasSinAsignar[tmpIndiceRamdom]];//asigno el sprite de la funcion respuesta
                        tmpImageRespuesta.SetNativeSize();
                        tmpImageRespuesta.gameObject.SetActive(true);
                    }
                    else
                    {
                        var tmpTextRespuesta = tmpContenedorRespuestas.GetChild(j).Find("TextRespuesta").GetComponent<TextMeshProUGUI>();
                        tmpTextRespuesta.text = DiccionarioIdiomas._instance.Traducir( refPreguntasSituacion._contenedorRespuestasFalsas[i]._respuestasFalsas[tmpListaIndicesRespuestasFalsasSinAsignar[tmpIndiceRamdom]], refPreguntasSituacion._contenedorRespuestasFalsas[i]._respuestasFalsas[tmpListaIndicesRespuestasFalsasSinAsignar[tmpIndiceRamdom]]);//asigno el texto de la respuesta correcta
                        tmpTextRespuesta.gameObject.SetActive(true);
                    }

                    //tmpContenedorRespuestas.GetChild(j).GetChild(1).GetComponent<Text>().text = ;//asigno una respuesta falsa con el indice ramdom
                    tmpListaIndicesRespuestasFalsasSinAsignar.RemoveAt(tmpIndiceRamdom);//quito el indice ramdom que ya fue usado de la lista
                }
            }

            //Activo el panel del enunciado y desactivo todo el resto de paneles de pregunta.
            for (int i = 0; i < panelesNavegacion.Length; i++)
            {
                if (i == 0)
                    panelesNavegacion[0].SetActive(true);
                else
                    panelesNavegacion[i].SetActive(false);
            }

            panelesPreguntaSinResponder = new bool[refPreguntasSituacion._preguntas.Length];

            for (int i = 0; i < panelesPreguntaSinResponder.Length; i++)
                panelesPreguntaSinResponder[i] = true;

            buttonTerminar.interactable = false;
            indicePanelNavegacionActual = 0;
        }

        /// <summary>
        /// Recorre todas las respuestas seleccioandas por el usuario y calcula la calificacion
        /// </summary>
        private void CalificarRespuestas()
        {
            /*
             -Determino la relacion de la calificacion, por ejemplo, si son 3 preguntas entonces 1 / 3 = valor de cada pregunta correcta
             -Para cada uno de los paneles de preguntas, miro todos los toggles y selecciono el que esta activado (el que el usuario selecciono como respuesta)
             -Luego comparo el indice de la posicion del toggle dentro de la gerarquia con el indice de donde deberia estar la respuesta correcta, si son iguales sumo calificion
             */

            var tmpCalificacionCadaRespuestaCorrecta = 1f / refPreguntasSituacion._preguntas.Length;
            var tmpSumaCalificacion = 0f;

            for (int i = 1; i < panelesNavegacion.Length; i++)
            {
                var tmpPadreTogglesRespuesta = panelesNavegacion[i].transform.GetChild(1);
                if (tmpPadreTogglesRespuesta.GetChild(indicesRespuestasCorrectas[i - 1]).GetComponent<Toggle>().isOn)
                    tmpSumaCalificacion += tmpCalificacionCadaRespuestaCorrecta;
            }

            refEvaluacion.AsignarPreguntasEvaluacionCorrectas(tmpSumaCalificacion);//asigno la calificacion

        }

        /// <summary>
        /// Consigue los textos de las respuestas de los toggles que el usuario selecciono
        /// </summary>
        private string[] GetRespuestasUsuario()
        {
            var tmpRespuesta = new string[panelesNavegacion.Length - 1];//menos 1 porque no se tiene en cuenta el panel del enunciado

            for (int i = 1; i < panelesNavegacion.Length; i++)
            {
                var tmpPadreTogglesRespuesta = panelesNavegacion[i].transform.GetChild(1);

                for (int j = 0; j < tmpPadreTogglesRespuesta.childCount; j++)
                {
                    var tmpToggle = tmpPadreTogglesRespuesta.GetChild(j).GetComponent<Toggle>();

                    if (tmpToggle.isOn)
                    {
                        tmpRespuesta[i - 1] = tmpToggle.transform.Find("TextRespuesta").GetComponent<TextMeshProUGUI>().text;
                        break;
                    }
                }

            }

            return tmpRespuesta;
        }

        /// <summary>
        /// Consigue los textos de las respuestas de los toggles que el usuario selecciono
        /// </summary>
        private Sprite[] GetRespuestasImagenUsuario()
        {
            var tmpRespuestaImagen = new Sprite[panelesNavegacion.Length - 1];//menos 1 porque no se tiene en cuenta el panel del enunciado

            for (int i = 1; i < panelesNavegacion.Length; i++)
            {
                var tmpPadreTogglesRespuesta = panelesNavegacion[i].transform.GetChild(1);

                for (int j = 0; j < tmpPadreTogglesRespuesta.childCount; j++)
                {
                    var tmpToggle = tmpPadreTogglesRespuesta.GetChild(j).GetComponent<Toggle>();

                    if (tmpToggle.isOn)
                    {
                        tmpRespuestaImagen[i - 1] = tmpToggle.transform.Find("ImageRespuesta").GetComponent<Image>().sprite;
                        break;
                    }
                }

            }

            return tmpRespuestaImagen;
        }

        private void ActivarBotonTerminar()
        {
            CalcularPanelPreguntaSinResponder();
            var tmpCantidadPanelesPreguntaSinResponder = 0;

            //Activar el boton terminar si todos los paneles fueron respondidos
            foreach (var tmpPanelPreguntaNoRespondido in panelesPreguntaSinResponder)
            {
                if (tmpPanelPreguntaNoRespondido)
                {
                    tmpCantidadPanelesPreguntaSinResponder++;
                }
            }

            if (tmpCantidadPanelesPreguntaSinResponder == 0)
                buttonTerminar.interactable = true;
        }

        #endregion

        #region public methods

        public void OnButtonDerecha()
        {
            CalcularPanelPreguntaSinResponder();
            panelesNavegacion[indicePanelNavegacionActual].SetActive(false);
            indicePanelNavegacionActual++;
            panelesNavegacion[indicePanelNavegacionActual].SetActive(true);
            CalcularEstadoBotonesDerechaIzquierda();
        }

        public void OnButtonIzquierda()
        {
            CalcularPanelPreguntaSinResponder();
            panelesNavegacion[indicePanelNavegacionActual].SetActive(false);
            indicePanelNavegacionActual--;
            panelesNavegacion[indicePanelNavegacionActual].SetActive(true);
            CalcularEstadoBotonesDerechaIzquierda();
        }

        public void OnButtonTerminar()
        {
            CalificarRespuestas();
            refControladorValoresPDF.SetPreguntasSituacion(refPreguntasSituacion, GetRespuestasUsuario(), GetRespuestasImagenUsuario());
            //refPanelInterfazCalificacionFinal.Mostrar(true);
            refCanvasReportePDF.CapturarImagenesPDF();
            Mostrar(false);


        }


        #endregion
    }
}