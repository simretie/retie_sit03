﻿using NSAvancedUI;
using NSBoxMessage;
using NsSeguridad;
using NSTraduccionIdiomas;
using System;
using UnityEngine;
using UnityEngine.UI;
using NSSingleton;

namespace NSInterfaz
{
    public class PanelInterfazLogin4Campos : AbstractPanelUIAnimation
    {
        #region members

        [SerializeField]
        private InputField inputNombre;

        [SerializeField]
        private InputField inputCurso;

        [SerializeField]
        private InputField inputIdCurso;

        [SerializeField]
        private InputField inputInstituto;

        private ClsSeguridad seguridad;

        private string[] datosLti;

        [SerializeField]
       
       

        private string[] cmd;

        private string correoMOno;

        private string nombreMono;

        private string instituMono;
           
        #endregion

        #region monoBehaviour

        private void Awake()
        {
            seguridad = GameObject.FindGameObjectWithTag("Seguridad").GetComponent<ClsSeguridad>();
        }

        private void Start()
        {
            seguridad.DlResLoginAula = loguin;

            inputNombre.text = DiccionarioIdiomas._instance.Traducir("TextPlaceholderLoguin", "");
            //inputNombre.placeholder.GetComponent<Text>().text = "";

            inputCurso.text = DiccionarioIdiomas._instance.Traducir("TextPlaceHolderCurso", "");
           // inputCurso.placeholder.GetComponent<Text>().text = "";

            inputIdCurso.text = DiccionarioIdiomas._instance.Traducir("TextPlaceholderIDCurso", "");
            //inputIdCurso.placeholder.GetComponent<Text>().text = "";

            inputInstituto.text = DiccionarioIdiomas._instance.Traducir("TextPlaceholderInstitucion", "");
           // inputInstituto.placeholder.GetComponent<Text>().text = "";

            datosLti = seguridad.GetDatosSesion();

            if (seguridad.modoMonoUsuario)
            {

#if UNITY_EDITOR || UNITY_STANDALONE
                // Obtenemos los argumentos enviados a la aplicación de escritorio.
                cmd = System.Environment.CommandLine.Split(',');
                try
                {
                    if (cmd.Length > 5)
                    {
                        nombreMono = cmd[3];
                        instituMono = cmd[4];
                        correoMOno = cmd[5];

                    }

                }
                catch
                {
                    BoxMessageManager._instance.MtdCreateBoxMessageInfo("esto es lo que hay dentro de la cmd " + cmd.ToString(), "ACEPTAR");
                }
#elif UNITY_ANDROID || UNITY_IPHONE
                try
                {
					AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"); 
					AndroidJavaObject currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

					if (currentActivity != null)
					{
                        AndroidJavaObject intent = currentActivity.Call<AndroidJavaObject>("getIntent");

                        if (intent != null)
                        {
                            nombreMono = safeCallStringMethod(intent, "getStringExtra", "nombre");
                            instituMono = safeCallStringMethod(intent, "getStringExtra", "institucion");
                            correoMOno = safeCallStringMethod(intent, "getStringExtra", "correo");
                        }
                    }
				}
                catch (Exception e)
                {
					Debug.Log(e.ToString());
				}
#endif
            }
        }

        private void Update()
        {
#if UNITY_WEBGL

            if (seguridad.LtiActivo)
            {
                inputNombre.text = datosLti[0];
                inputCurso.text = datosLti[1];
                inputIdCurso.text = datosLti[2];
                inputInstituto.text = datosLti[3];
            }

#elif UNITY_ANDROID || UNITY_IPHONE|| UNITY_EDITOR || UNITY_STANDALONE

            if (seguridad.modoMonoUsuario)
            {
                inputNombre.text = nombreMono;
                inputInstituto.text = instituMono;
            }
#endif
        }
        #endregion

        #region static methods

        public static string safeCallStringMethod(AndroidJavaObject javaObject, string methodName, params object[] args)
        {
            if (args == null)
                args = new object[] { null };

            IntPtr methodID = AndroidJNIHelper.GetMethodID<string>(javaObject.GetRawClass(), methodName, args, false);
            jvalue[] jniArgs = AndroidJNIHelper.CreateJNIArgArray(args);

            try
            {
                IntPtr returnValue = AndroidJNI.CallObjectMethod(javaObject.GetRawObject(), methodID, jniArgs);
                if (IntPtr.Zero != returnValue)
                {
                    var val = AndroidJNI.GetStringUTFChars(returnValue);
                    AndroidJNI.DeleteLocalRef(returnValue);
                    return val;
                }
            }
            finally
            {
                AndroidJNIHelper.DeleteJNIArgArray(args, jniArgs);
            }

            return null;
        }

        #endregion

        #region public methods

        public void loguin(int op)
        {
            if (op == 0)
                Mostrar(false);
        }

        /// <summary>
        /// metodo que activa el inicio de sesion offline
        /// </summary>
        public void BtnIniciarOffline()
        {
           
            if (inputNombre.text != "" && inputCurso.text != "" && inputInstituto.text != "" && inputIdCurso.text != "")
            {
                Debug.Log("entre a llamar aula loginRequest");
                seguridad.MtdLoguinOffLine(inputNombre.text, inputCurso.text, inputIdCurso.text, inputInstituto.text);
            }
            else            
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeLLenarcampos", "Todos los campos son requeridos."), DiccionarioIdiomas._instance.Traducir("TextBotonAceptar", "ACEPTAR"));            
        }
        #endregion
    }
}